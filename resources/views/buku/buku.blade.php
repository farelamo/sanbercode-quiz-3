@extends('layouts.master')

@section('judul')
    <h1> Halaman Buku </h1><a href="/buku/create" class="btn btn-primary">Tambah</a>
@endsection

@section('isi')
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Pengarang</th>
            <th scope="col">tahun</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key=>$value)
            <tr>
                <td>{{ $key + 1 }}</th>
                <td>{{ $value->judul }}</td>
                <td>{{ $value->deskripsi }}</td>
                <td>{{ $value->pengarang }}</td>
                <td>{{ $value->tahun }}</td>
                <td>
                    <a href="/buku/{{ $value->id }}" class="btn btn-info">Show</a>
                    <a href="/buku/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                    <form action="/buku/{{ $value->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection

@push('scripts')

<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>


@endpush