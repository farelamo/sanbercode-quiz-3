@extends('layouts.master')

@section('judul')
    Halaman Detail Buku
@endsection



@section('isi')

    <h3>{{ $buku->judul }}</h3>
    <p> Deskripsi : {{ $buku->deskripsi }}</p>
    <p>Pengarang : {{ $buku->pengarang }}</p>
    <p>Tahun : {{ $buku->tahun }}</p>

    Genre
    <ul>
        @forelse ($genre as $value)
            <li>{{ $value->nama }}</li>
        @empty
            <li>No data</li>
        @endforelse
    </ul>

@endsection
