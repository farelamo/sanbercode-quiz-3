@extends('layouts.master')

@section('judul')
    <h1> Halaman Create Buku </h1>
@endsection

@section('isi')
<form action="/buku/{{$buku->id}}" method="post">
    @method('PUT')
    @csrf
    <div class="mb-3">
        <label for="Judul" class="form-label">Judul</label>
        <input type="text" class="form-control" id="Judul" name="judul" value="{{$buku->judul}}">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="Deskripsi" class="form-label">Deskripsi</label></label>
        <textarea class="form-control" id="Deskripsi" rows="3" name="deskripsi">{{$buku->deskripsi}}</textarea>
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="Pengarang" class="form-label">Pengarang</label></label>
        <textarea class="form-control" id="Pengarang" rows="3" name="pengarang">{{$buku->pengarang}}</textarea>
        @error('pengarang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="Tahun" class="form-label">Tahun</label></label>
        <textarea class="form-control" id="Tahun" rows="3" name="tahun">{{$buku->tahun}}</textarea>
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button class="btn btn-primary" type="submit">Edit Data</button>
</form>
@endsection